---
layout: post  
title: "La Aventura de la Depresión"  
date: 2020-08-14
categories: podcast
image: http://cataclismospodcast.gitlab.io/images/14.png
podcast_link: https://archive.org/download/la-aventura-de-la-depresion/14.La%20Aventura%20de%20la%20Depresion
tags: [audio, lgbt, cultura, Cataclismos]  
comments: true 
---
Temporada 2: Ámate bien y bonito.

*Es difícil describir lo que es una depresión a quien nunca lo ha vivido. En este episodio te cuento mi experiencia.
Como te mencioné en el episodio dejo los números telefónicos y nombres de organismos que pueden orientarte si estás pasándola mal de momento.
No estás solo. Recuerda que tu salud mental es igual de importante que tu salud física.

-Alumbra By Early Institute- Tel. (55) 88 54 66 53 (Atención de Lunes a Viernes de 8:00 A.M. a 8:00 P.M.)
Chat en Línea- https://alumbramx.org/ayuda/

-Consejo Ciudadano para la Seguridad y Justicia de la Ciudad de México - Línea de Seguridad o Chat de Confianza 55 5533-5533 (Atención 24/7) 

-SAPTEL: (55) 5259-81-21 (Servicio gratuito 24/7)

<audio controls>
  <source src="https://archive.org/download/la-aventura-de-la-depresion/14.La%20Aventura%20de%20la%20Depresion.ogg" type="audio/ogg">
  <source src="https://archive.org/download/la-aventura-de-la-depresion/14.La%20Aventura%20de%20la%20Depresion.mp3" type="audio/mpeg">
</audio>

![#depresion](http://cataclismospodcast.gitlab.io/images/14.png)

Recuerda que puedes **contactar** e **interactuar** con nosotros de las siguientes formas:

+ Correo: <cataclismospodcast@outlook.com>
+ Web: <https://cataclismospodcast.gitlab.io/>
+ Feed Podcast: <https://cataclismospodcast.gitlab.io/feed>