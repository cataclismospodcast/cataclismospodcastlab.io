---
layout: post  
title: "Drag Queen: Arte e Identidad (Amitaí Verdugo)"  
date: 2020-08-07
categories: podcast
image: http://cataclismospodcast.gitlab.io/images/13.png
podcast_link: https://archive.org/download/drag-queen/DragQueensMx
tags: [audio, lgbt, cultura, Cataclismos]  
comments: true 
---
Temporada 2: Ámate bien y bonito.
*El Drag resulta por sí sola una expresión de arte que todos podemos disfrutar; no es coincidencia que esté resurgiendo como movimiento de arte y liberación a nivel mundial.

En este episodio Amitaí Verdugo, una promesa del Drag Queen mexicano nos narra su perspectiva sobre esta fascinante y glamourosa cultura. 

<audio controls>
  <source src="https://archive.org/download/drag-queen/DragQueensMx.ogg" type="audio/ogg">
  <source src="https://archive.org/download/drag-queen/DragQueensMx.mp3" type="audio/mpeg">
</audio>

![#Muxe](http://cataclismospodcast.gitlab.io/images/13.png)

Recuerda que puedes **contactar** e **interactuar** con nosotros de las siguientes formas:

+ Correo: <cataclismospodcast@outlook.com>
+ Web: <https://cataclismospodcast.gitlab.io/>
+ Feed Podcast: <https://cataclismospodcast.gitlab.io/feed>