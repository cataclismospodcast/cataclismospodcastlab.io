---
layout: post  
title: "Ámate Bien y Bonito"  
date: 2020-06-26
categories: podcast
image: http://cataclismospodcast.gitlab.io/images/07.png
podcast_link: https://archive.org/download/orgullo-interludio/07.%20Orgullo%20Interludio
tags: [audio, lgbt, cultura, Cataclismos]  
comments: true 
---
Temporada 2: 
*Sé quien eres y di lo que sientes, porque aquellos a quienes les molesta no importan, y a quienes les importas no les molesta. Ámate como eres, nada te hace menos valioso, nada, recuérdalo siempre. 

<audio controls>
  <source src="https://archive.org/download/orgullo-interludio/07.%20Orgullo%20Interludio.ogg" type="audio/ogg">
  <source src="https://archive.org/download/orgullo-interludio/07.%20Orgullo%20Interludio.mp3" type="audio/mpeg">
</audio>

![#Piloto](http://cataclismospodcast.gitlab.io/images/07.png)

Recuerda que puedes **contactar** e **interactuar** con nosotros de las siguientes formas:

+ Correo: <cataclismospodcast@outlook.com>
+ Web: <https://cataclismospodcast.gitlab.io/>
+ Feed Podcast: <https://cataclismospodcast.gitlab.io/feed>
