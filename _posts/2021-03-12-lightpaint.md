---
layout: post  
title: "Light Painting: Creatividad y Técnica (Iván Lucio)"  
date: 2021-03-12
categories: podcast  
image: http://cataclismospodcast.gitlab.io/images/37.png
podcast_link: https://archive.org/download/lightpainting/LIGHTPAINTING%20Ivan
tags: [audio, lightpainting, cataclismos]  
comments: true
---
Iván Lucio es fotógrafo de larga exposición y apasionado del arte Light Painting. Tiene una trayectoria de más de 10, es miembro y fundador de la firma Riders Of Light y alterna trabajos en solitario con trabajos en grupo y colaboraciones. Sin duda alguna, ya se ha convertido en un referente del medio. Y hoy nos cuenta un poco más sobre su historia, inspiración y experiencias.


<audio controls>
  <source src="https://archive.org/download/lightpainting/LIGHTPAINTING%20Ivan.ogg" type="audio/ogg">
  <source src="https://archive.org/download/lightpainting/LIGHTPAINTING%20Ivan.mp3" type="audio/mpeg">
</audio>

![#Lucio](http://cataclismospodcast.gitlab.io/images/37.png)

Recuerda que puedes **contactar** e **interactuar** con nosotros de las siguientes formas:

+ Instagram: <https://www.instagram.com/alam.dnl/>
+ <https://www.instagram.com/cataclismos_podcast/>
+ Correo: <cataclismospodcast@outlook.com>
