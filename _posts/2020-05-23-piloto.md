---
layout: post  
title: "Episodio Piloto"  
date: 2020-05-23  
categories: podcast  
image: http://cataclismospodcast.gitlab.io/images/00.png
podcast_link: https://archive.org/download/piloto01/Episodio%20Piloto
tags: [audio, piloto, cultura, Cataclismos]  
comments: true 
---
Hola amiwos. En este episodio hablaré de qué va el podcast y sí, porque estaba inspirado y de buenas, una pequeña reflexión sobre la vida moderna. Espero que disfruten esta primera creación y que no sea la última vez que nos escuchan. Los amo <3. 

<audio controls>
  <source src="https://archive.org/download/piloto01/Episodio%20Piloto.ogg" type="audio/ogg">
  <source src="https://archive.org/download/piloto01/Episodio%20Piloto%20%281%29.mp3" type="audio/mpeg">
</audio>

![#Piloto](http://cataclismospodcast.gitlab.io/images/00.png)

Recuerda que puedes **contactar** e **interactuar** con nosotros de las siguientes formas:

+ Correo: <cataclismospodcast@outlook.com>
+ Web: <https://cataclismospodcast.gitlab.io/>
+ Feed Podcast: <https://cataclismospodcast.gitlab.io/feed>
